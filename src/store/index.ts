import { ref } from "vue";
import { createWorker } from "tesseract.js";

export const loading = ref("");
export const isEditable = ref(false);

export const selectedLang = ref("en");

export const ocrImage = ref("");
export const recognizedText = ref("");

export const translatedText = ref("");

export const copyText = (text: string) => {
  const textArea = document.createElement("textarea");
  textArea.value = text;
  document.body.appendChild(textArea);
  textArea.select();
  document.execCommand("copy");
  document.body.removeChild(textArea);
};

export const recognizeText = async (file: any) => {
  if (!file) return;
  ocrImage.value = URL.createObjectURL(file);
  recognizedText.value = null;
  const worker = await createWorker();
  try {
    await worker.load();
    // await worker.loadLanguage("eng");
    // await worker.initialize("eng");
    const {
      data: { text },
    } = await worker.recognize(file, {});
    recognizedText.value = text;
    return text;
  } catch (error) {
    console.error("OCR error:", error);
  } finally {
    await worker.terminate();
  }
};

export const translateText = async (text) => {
  try {
    const response = await fetch(
      "https://translation.googleapis.com/language/translate/v2?key=AIzaSyDaGCLq0Rl3squR_qDFvYW-q3ZO7ZvpVkQ",
      {
        method: "POST",
        body: JSON.stringify({
          q: text,
          source: "en",
          target: "hi", // Change the target language as needed (e.g., 'es' for Spanish)
          format: "html",
        }),
      }
    );

    if (!response.ok) {
      throw new Error("Translation request failed");
    }

    const data = await response.json();
    return data.data.translations[0].translatedText as string;
  } catch (error) {
    console.error("Translation error:", error);
  }
};
