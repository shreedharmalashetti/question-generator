import { loading } from ".";

export const uploadImage = async (file: any) => {
  try {
    const formdata = new FormData();
    formdata.append("file", file, "001.png");

    const response = await fetch(
      // "https://e2dd-2401-4900-1c5b-660f-a5f4-9984-fff6-6e0e.ngrok.io/v1/files",
      "https://api.penpencil.co/v1/files",
      {
        method: "POST",
        body: formdata,
      }
    );

    if (!response.ok) {
      console.log(response);
      return null;
    }

    const { data } = await response.json();
    if (!data) return null;
    return data.baseUrl + data.key;
  } catch (error) {
    console.log(error);
  }
};
