import { ref } from "vue";
import { translateText } from ".";

export interface ITexts {
  en: string;
  hi: string;
}
export interface IQuestion {
  _id?: string;
  texts: ITexts;
  type: "Single" | "Mltiple" | "Numeric";
  positiveMarks: number;
  negativeMarks: number;
  displayOrder: number;
  options?: {
    _id?: string;
    texts: ITexts;
    isSolution?: boolean;
  }[];
  numericAnswer?: number;
  id?: number; //for draggable
}
export interface IQuestionGroup {
  _id?: string;
  name: string;
  questions: IQuestion[];
}

export const getQuestions = () => {
  return [
    {
      type: "Single",
      positiveMarks: 4,
      negativeMarks: 4,
      texts: {
        en: "text 1",
        hi: "",
      },
      displayOrder: 1,
      options: [
        {
          texts: {
            en: "option 1",
          },
          isSolution: false,
        },
        {
          texts: {
            en: "option 2",
          },
        },
        {
          texts: {
            en: "option 3",
          },
          isSolution: true,
        },
      ],
    },
    {
      type: "Single",
      positiveMarks: 4,
      negativeMarks: 4,
      texts: {
        en: "text 2",
        hi: "",
      },
      displayOrder: 2,
    },
    {
      type: "Single",
      positiveMarks: 4,
      negativeMarks: 4,
      texts: {
        en: "text 3",
        hi: "",
      },
      displayOrder: 3,
    },
  ] as IQuestion[];
};

export const questionGroups = ref<IQuestionGroup[]>([]);

export const getId = (gId: number, qId: number) => {
  let id = 0;
  for (let i = 0; i < gId; i++) {
    id += questionGroups.value[i].questions.length;
  }
  id += qId + 1;
  return id;
};
export const getQuestionGroups = async () => {
  if (questionGroups.value.length) return;
  questionGroups.value = [
    {
      _id: "", //combination of section ans subject
      name: "section1->subject1",
      questions: getQuestions(),
    },
    {
      _id: "", //combination of section ans subject
      name: "section1->subject2",
      questions: getQuestions(),
    },
  ];
  questionGroups.value = questionGroups.value.map((g, index) => {
    g.questions.forEach((q, i) => {
      q.displayOrder = getId(index, i);
      q.id = getId(index, i);
    });
    return g;
  });
};

export const translateToHindi = async () => {
  await Promise.all(
    questionGroups.value.map(async (qGroup) => {
      await Promise.all(
        qGroup.questions.map(async (question) => {
          if (question.texts?.en) {
            question.texts.hi = await translateText(question.texts.en);
          }
          if (question.options?.length) {
            await Promise.all(
              question.options.map(async (option) => {
                if (option.texts?.en) {
                  option.texts.hi = await translateText(option.texts.en);
                }
              })
            );
          }
        })
      );
    })
  );
  alert("translated all questions text to hindi");
};
